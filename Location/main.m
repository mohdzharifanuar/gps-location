//
//  main.m
//  Location
//
//  Created by Mohd Zharif Anuar on 12/17/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
