//
//  AppDelegate.h
//  Location
//
//  Created by Mohd Zharif Anuar on 12/17/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

